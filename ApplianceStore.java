import java.util.Scanner;
public class ApplianceStore {
	
	public static void main (String[] args) {
		Stove[] stoves = new Stove[4];
		Scanner r = new Scanner(System.in);
		for (int i = 0; i < 4; i++) {
			
			System.out.println("What price is your stove ?");
			double price = r.nextDouble();
			
			System.out.println("What brand is your stove ?");
			String brand = r.next();
			
			System.out.println("What is your stove type ?");
			String type = r.next();
			
			stoves[i] = new Stove(price, brand, type);
		
		}
		
		System.out.println("Price of your stove before update : "+stoves[3].getPrice());
		System.out.println("Brand of your stove before update : "+stoves[3].getBrand());
		System.out.println("Type of your stove before update : "+stoves[3].getStoveType());
		
		System.out.println("Update the price of your last stove");
		stoves[3].setPrice(r.nextDouble());
			
		System.out.println("Update the brand of your last stove");
		stoves[3].setBrand(r.next());
			
		System.out.println("Update your stove's type");
		stoves[3].setStoveType(r.next());
		
		System.out.println("Price of your stove after update : "+stoves[3].getPrice());
		System.out.println("Brand of your stove after update : "+stoves[3].getBrand());
		System.out.println("Type of your stove after update : "+stoves[3].getStoveType());
		
	
		
		System.out.println("How many burners does your first stove have ?");
		int numberOfBurners = r.nextInt();
		
		System.out.println("How many seconds would you like to heat your food for ? (can only be up to 60 seconds)");
		int time = r.nextInt();
		
		System.out.println("Stove price : "+stoves[3].getPrice());
		System.out.println("Stove brand : "+stoves[3].getBrand());
		System.out.println("Stove type : "+stoves[3].getStoveType());
		
		stoves[0].printBrand();
		stoves[0].recommendedPrice(numberOfBurners);
		
		stoves[0].heatYourFood(time);
	}
}

