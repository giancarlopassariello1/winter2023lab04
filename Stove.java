public class Stove {
	private double price;
	private String brand;
	private String stoveType;
	private int amountOfBurners;
	private int amountOfTime;
	
	public void printBrand() {
		System.out.println("Your stove's brand is : "+this.brand);
	}
	
	public void recommendedPrice(int amountOfBurners) {
		if (amountOfBurners < 1) {
			System.out.println("I don't recommend a stove with no burners ! ");
		} else if (amountOfBurners <= 2) {
			this.price = (this.price + 500) / 2;
			System.out.println("The recommended price for your stove based on the amount of burners you have is "+this.price);
		} else if (amountOfBurners > 2 && amountOfBurners <= 4) {
			this.price = (this.price + 1000) / 2;
			System.out.println("The recommended price for your stove based on the amount of burners you have is "+this.price);
		} else {
			this.price = (this.price + 1600) / 2;
			System.out.println("The recommended price for your stove based on the amount of burners you have is "+this.price);
		}		
	}
	
	public void heatYourFood(int amountOfTime) {
		if (isTimeValid(amountOfTime)) {
			for (int i = amountOfTime; i >= 0; i-=1) {

				System.out.println("Time : "+i);
			}
			System.out.println("Enjoy your food ! :) ");
		} else {
			System.out.println("It is impossible to heat your food for this amount of time !");
		}
	}
	
	private boolean isTimeValid(int amountOfTime) {
		boolean isTheTimeValid = (amountOfTime >= 1) && (amountOfTime <= 60);
		return isTheTimeValid;
	}
	
	public void setPrice(double newPrice) {
		this.price = newPrice;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setBrand(String newBrand) {
		this.brand = newBrand;
	}
	
	public String getBrand() {
		return this.brand;
	}
	
	public void setStoveType(String newStoveType) {
		this.stoveType = newStoveType;
	}
	
	public String getStoveType() {
		return this.stoveType;
	}
	
	public void setAmountOfBurners(int newAmountOfBurners) {
		this.amountOfBurners = newAmountOfBurners;
	}
	
	public int getAmountOfBurners() {
		return this.amountOfBurners;
	}
	
	public void setAmountOfTime(int newAmountOfTime) {
		this.amountOfTime = newAmountOfTime;
	}
	
	public int getAmountOfTime() {
		return this.amountOfTime;
	}	
	
	public Stove(double newPrice, String newBrand, String newStoveType) {
		this.price = newPrice;
		this.brand = newBrand;
		this.stoveType = newStoveType;
	}
}

